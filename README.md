# GitHub Mobile Code Wrap
[![License: MIT](https://img.shields.io/badge/License-MIT-97CA00.svg?style=flat-square)](./LICENSE)
[![Mozilla Add-on](https://img.shields.io/amo/v/github-mobile-code-wrap.svg?style=flat-square)](https://addons.mozilla.org/en-US/firefox/addon/github-mobile-code-wrap/)

A Firefox add-on to wrap long line code in GitHub mobile site.
