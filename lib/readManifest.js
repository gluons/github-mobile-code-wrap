const { readFile } = require('fs');
const Mustache = require('mustache');
const { resolve } = require('path');
const { promisify } = require('util');

const pkg = require('../package.json');
const pReadFile = promisify(readFile);

const manifestFile = resolve(__dirname, '../manifest.json');

/**
 * Read and render `manifest.json` file.
 *
 * @returns {Promise<string>}
 */
module.exports = () => pReadFile(manifestFile, 'utf8').then(content => Mustache.render(content, pkg));
