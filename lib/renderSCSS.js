const sass = require('sass');

/**
 * Render SCSS file to CSS. (with sourcemap)
 *
 * @param {string} srcFile
 * @param {string} destFile
 * @returns {Promise<{ css: Buffer, map: Buffer, stats: object }>}
 */
module.exports = (srcFile, destFile) => new Promise((resolve, reject) => {
	sass.render(
		{
			file: srcFile,
			outputStyle: 'compressed',
			outFile: destFile,
			sourceMap: true
		},
		(err, result) => {
			if (err) {
				reject(err);
			} else {
				resolve(result);
			}
		}
	);
});
