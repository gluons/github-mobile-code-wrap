const chokidar = require('chokidar');
const { resolve } = require('path');
const { watch } = require('signale');

const build = require('./build');

const watcher = chokidar.watch('src/*', {
	cwd: resolve(__dirname, '..')
});

watch('Watching source directory...');

watcher.on('change', () => {
	build();
});
