const del = require('del');
const { writeFile } = require('fs');
const makeDir = require('make-dir');
const { resolve } = require('path');
const { complete, success, error } = require('signale');
const { promisify } = require('util');

const pWriteFile = promisify(writeFile);

const renderSCSS = require('../lib/renderSCSS');
const readManifest = require('../lib/readManifest');

const distPath = resolve(__dirname, '../dist');
const srcFile = resolve(__dirname, '../src/main.scss');
const destManifestFile = resolve(__dirname, '../dist/manifest.json');
const destFile = resolve(__dirname, '../dist/main.css');
const destMapFile = `${destFile}.map`;

async function build() {
	try {
		await makeDir(distPath);

		await del(`dist/*`, {
			cwd: resolve(__dirname, '..'),
			force: true
		});
		complete('Dist cleaned.');

		const [SCSSResult, manifestContent] = await Promise.all([
			renderSCSS(srcFile, destFile),
			readManifest()
		]);
		const { css, map } = SCSSResult;

		await Promise.all([
			pWriteFile(destFile, css, 'utf8'),
			pWriteFile(destMapFile, map, 'utf8'),
			pWriteFile(destManifestFile, manifestContent, 'utf8')
		]);

		success('Build succeed.');
	} catch (err) {
		error(err);
	}
}

build();

module.exports = build;
